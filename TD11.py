def plus_long_enchainement(liste):
    """retourne la plus grande suite de nombre consécutif dans la liste

    Args:
        liste (list): une liste 

    Returns:
        list: la plus grande suite
    """
    actual_suite = []
    res = []
    liste.sort()
    for element in liste:
        if actual_suite == [] or (actual_suite[-1] + 1) == element or actual_suite[-1] == element:
            if actual_suite == [] or actual_suite[-1] != element:
                actual_suite.append(element)
        else:
            if res == [] or len(actual_suite) > len(res):
                res = actual_suite
                actual_suite = [element]
    return res

assert plus_long_enchainement([1,45,54,8,6546,8768,75,46,47,4,5,6,7,8,9,10,11,11,12,13,4]) == [4,5,6,7,8,9,10,11,12,13]
assert plus_long_enchainement([]) == []

def ajoute(nombre, liste):
    """ajoute un nombre à une liste trié par ordre croissant au bon endroit

    Args:
        nombre (int): Un no
        liste (list): Une liste trié dans l'ordre croissant
    """

assert ajoute(3, [1, 2, 5, 6]) == [1, 2, 3, 5, 6]