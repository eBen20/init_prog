
"""
              Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module protection.py
    gère les protections que les joueurs peuvent utiliser
"""

DPO = 0
FIREWALL_BDHG = 1
FIREWALL_BGHD = 2
DONNEES_PERSONNELLES = 3
ANTIVIRUS = 4
PAS_DE_PROTECTION = 5

RESISTANCE = 2


def creer_protection(type_p, resistance):
    """créer une protection

    Args:
        type_p (int): type de la protection
        resistance (int): nombre d'attaques que peut supporter la protection

    Returns:
        dict: une protection
    """
    if type_p in (DPO, FIREWALL_BDHG, FIREWALL_BGHD, DONNEES_PERSONNELLES, ANTIVIRUS, PAS_DE_PROTECTION) and isinstance(resistance, int):
        return {"type": type_p, "resistance": resistance}
    return None 

def get_type(protection):
    """retourne le type de la protection

    Args:
        protection (dict): une protection

    Returns:
        int: le type de la protection
    """
    if protection is not None:
        return protection["type"]
    return None

def get_resistance(protection):
    """retourne la résistance de la protection

    Args:
        protection (dict): une protection

    Returns:
        int: la resistance de la protection
    """
    if protection is not None:
        return protection["resistance"]
    return None

def enlever_resistance(protection):
    """Enlève un point de résistance de la protection et retourne la resistance restante

    Args:
        protection (dict): une protection

    Returns:
        int: la resistance restante
    """
    if protection is not None:
        protection["resistance"] -= 1
        if protection["resistance"] < 0:
            protection["resistance"] = 0
        return get_resistance(protection)
    return None