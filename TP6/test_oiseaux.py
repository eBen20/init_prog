import oiseaux
# --------------------------------------
# FONCTIONS
# --------------------------------------

def test_recherche_oiseau():
    assert oiseaux.recherche_oiseau("Merle",oiseaux.OISEAUX)== ("Merle","Turtidé")
#
def test_recherche_par_famille():
    assert oiseaux.recherche_par_famille("Turtidé",oiseaux.OISEAUX)==["Merle"]

def test_oiseau_le_plus_observe():
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations1)=="Moineau"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations2)=="Tourterelle"
    assert oiseaux.oiseau_le_plus_observe(oiseaux.observations3)=="Mésange"
    assert oiseaux.oiseau_le_plus_observe([])==None

def test_est_liste_observations():
    assert oiseaux.est_liste_observations(oiseaux.observations1) == True

def test_max_observations():
    assert oiseaux.max_observations(oiseaux.observations1) == "Moineau"

def test_moyenne_oiseaux_observes():
    assert oiseaux.moyenne_oiseaux_observes(oiseaux.observations1)== 3.0

def test_total_famille():
    assert oiseaux.total_famille(oiseaux.observations1,oiseaux.OISEAUX,"Turtidé") == 2


def test_construire_liste_observations():
    assert oiseaux.construire_liste_observationse(oiseaux.OISEAUX,oiseaux.comptage1) == oiseaux.observations1

#def test_creer_ligne_sup():
   assert oiseaux.creer_ligne_sup(...)==...

#def test_creer_ligne_noms_oiseaux():
 #   assert oiseaux.creer_ligne_noms_oiseaux(...)==...



