﻿#!/bin/bash

sudo apt update || passudo=1

if [$passudo = 0]
then
    python3.8 --version || echo  y |  sudo apt install python3.8 ||  zenity --info --title Erreur --text "python ne peut pas être installé"
    java -version || echo  y |sudo apt install default-jre default-jdk|| zenity --info --title Erreur --text "java ne peut pas être installé"
    docker.io -v || echo  y | sudo apt install docker.io || zenity --info --title Erreur --text "docker.io ne peut pas être installé"
    sudo systemctl start docker
    code -v || echo  y | sudo apt install code || zenity --info --title Erreur --text "code ne peut pas être installé"
    git --version | echo  y | sudo apt install git-all 
    firefox --version || echo y |  sudo apt install firefox ||  zenity --info --title Erreur --text "firefox ne peut pas être installé"
fi
#Configuration git
git config --global user.email "benjamin.guerre@etu-orleans.fr"
git config --global user.name "eBen20"

#installation extetions  vscode
code --install-extension ms-python.python
code --install-extension njpwerner.autodocstring
code --install-extension gitlab.gitlab-workflow
code --install-extension vscjava.vscode-java-pack
code --install-extension ms-mssql.sql-database-projects-vscode
code --install-extension tomoki1207.pdf
code --install-extension pierrequemard.macro


