#exercice 1
def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max=0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle=0 #longueur du plateau actuel
    if len(chaine)!=0:
        lg_actuelle=1 

    for lettre in range(1,len(chaine)):
        if chaine[lettre-1]==chaine[lettre]: # si la lettre actuelle est égale à la précédente
            lg_actuelle+=1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle>lg_max:
                lg_max=lg_actuelle
            lg_actuelle=1

    if lg_actuelle>lg_max: #cas du dernier plateau
        lg_max=lg_actuelle
    return lg_max

plus_long_plateau("aaaiugdsfiue")

def test_plus_long_plateau():
    assert plus_long_plateau("aaaiugdsfiue") == 3
    assert plus_long_plateau("aabbbcdddd") == 4
    assert plus_long_plateau("a") == 1
    assert plus_long_plateau("aaaaaaa") == 7
    assert plus_long_plateau("aaaabbbbba") == 5

#---------------------------------------------------------------------------------
#Ex2

# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

def ville_plus_peuple(listenomville,listenbhabitant):
    """Donne la ville la plus peuplé en utilisant une liste de nom de ville et 
    une autre avec le nombres d'habitants des villes situer au même endroit dans la liste

    Args:
        listenomville (list): Une liste de nom de ville
        listenbhabitant (list): Une liste de nombre d'habitant

    Returns:
        str: Le nom de la ville avec le plus d'habitant
    """    
    max_peuple = 0
    ville_max_peuple = ''

    #ind est la ville qu'on est en train d'étudier
    for ind in range(len(listenbhabitant)):
        if listenbhabitant[ind] > max_peuple:
            max_peuple = listenbhabitant[ind]
            ville_max_peuple = listenomville[ind]
    return ville_max_peuple

#correction
def ville_population_max(liste_ville,liste_population):
    """Donnne la ville la plus peuplé en utilisant une liste de nom de ville et 
        une autre avec le nombres d'habitants des villes situer au même endroit dans la liste

    Args:
        liste_ville (list): Une liste de nom de ville
        liste_population (list): Une liste de nombre d'habitant

    Returns:
        str: Le nom de la ville avec le plus d'habitant
        None : si erreur
    """  
    if len(liste_ville) != len(liste_population):
        res =None
    else:
        i_max = None
        for indice in range(len(liste_population)):
            if i_max == None or liste_population[indice] > liste_population[i_max]:
                i_max = indice
        if i_max == None:
            res = None * liste_population[i_max]
        else:
            res = liste_ville[i_max]
    return res


def test_ville_plus_peuple():
    assert ville_plus_peuple(liste_villes,population) == 'Tours'
    assert ville_plus_peuple([],[]) == ''

def test_ville_population_max():
    assert ville_plus_peuple(liste_villes,population) == 'Tours'
    assert ville_plus_peuple([],[]) == ''


#----------------------------------------------------------------------------------------------------------------------

#Ex3

def int2(chaine):
    """Transforme un nombre écrit str en int

    Args:
        chaine (str): Un nombre en str  

    Returns:
        int: Le nombre transformé en int
    """ 
    if len(chaine) == 0:
        res = None
    else:
        res = 0
        multiplicateur = 10**(len(chaine)-1)
        for indice in range(len(chaine)):
            if chaine[indice] == '1':
                res += 1*multiplicateur
            if chaine[indice] == '2':
                res += 2*multiplicateur
            if chaine[indice] == '3':
                res += 3*multiplicateur
            if chaine[indice] == '4':
                res += 4*multiplicateur
            if chaine[indice] == '5':
                res += 5*multiplicateur
            if chaine[indice] == '6':
                res += 6*multiplicateur
            if chaine[indice] == '7':
                res += 7*multiplicateur
            if chaine[indice] == '8':
                res += 8*multiplicateur
            if chaine[indice] == '9':
                res += 9*multiplicateur
            if chaine[indice] == '0':
                res += 0*multiplicateur
            multiplicateur //= 10
    if chaine[0] == '-':
        res = -res
    return res

def test_int2():
    assert int2('2021') == 2021
    assert int2('0') == 0
    assert int2('-21') == -21
    assert int2('11') == 11
#-------------------------------------------------------------------------------------------------------------------------

#Ex4

def lettre_debut_mot(lettre,liste):
    """Trouve les mots commençant par lettre 
    Args:
        liste (list): Une liste de mots
        lettre (str): La lettre avec la quel les mots doivent comencé 

    Returns:
        list: La liste des mots commençant par lettre
    """  
    res = []
    for indice in range(len(liste)):
        if liste[indice][0] == lettre:
            res.append(liste[indice])
    return res

def test_lettre_debut_mot():
    assert lettre_debut_mot('h',["salut","hello","hallo","ciao","hola"]) == ["hello","hallo","hola"]
    assert lettre_debut_mot('a',["salut","hello","hallo","ciao","hola"]) == []
    assert lettre_debut_mot('i',['ouististi']) == []
    assert lettre_debut_mot('k',['allo']) == []

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#Ex5
def decoupage_mot(chaine):
    '''Donne la liste de mots dans une chaine de caractère
    Args:
        chaine (str): Une phrase
    
    Returns:
        list: retourne la liste des mots dans la chaine de caractère
    '''
    res = []
    mot = ''                     #va stocker le mot où l'on recupère les lettres
    for indice in range(len(chaine)): #on parcourt les indices de la chaine
        if chaine[indice].isalpha(): #on verif c le caratère est une lettre
            mot += chaine[indice] #on add la lettre au mot en cours 
        else:
            if len(mot) != 0:   #si on a un mot
                res.append(mot) #on add ce mot au résultat
            mot = ''            #on efface le mot en cour   
    if mot != '':
        res.append(mot)
    return res
decoupage_mot("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!")

def test_decoupage_mot():
    assert decoupage_mot("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!") == ["Cela","fait","déjà","jours","jours","à","l","IUT","O","Cool"]
    assert decoupage_mot("(3*2)+1") == []
    assert decoupage_mot("un deux 3") == ['un','deux']
    assert decoupage_mot('') == []
    

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#Ex6
def mot_start_lettre(texte, lettre):
    """Donne une liste avec les mots du texte commençant par la lettre donné

    Args:
        texte (list): liste de mots 
        lettre (str): lettre 

    Returns:
        list: liste des mots comançant par la lettre
    """    
    res =[]
    liste_mot = decoupage_mot(texte)
    for indice in range(len(liste_mot)):   #parcourt les indice de la liste 
        if liste_mot[indice][0] == lettre: #Si le premier caractère du mot en cour de traitement est la lettre voulu 
            res.append(liste_mot[indice])  #On ajoute le mot au résultat
    return res

def test_mot_start_lettre():
    assert mot_start_lettre("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!" , 'C') == ["Cela","Cool"]
    assert mot_start_lettre(" ","A") == []
    assert mot_start_lettre("un deux trois","M") == []
    assert mot_start_lettre("Mardi je mange" ,"M") == ["Mardi"]

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#Ex7
#mettre prendre un nombre , a False tt les multiples du nombre puis nombre suivant à True sup .... ect...
#dans la boucle on fait une boucle 

def creer(longueur):
    """Creer  un liste de longeur longeur avec les deux premier éléments F et les autres T

    Args:
        longueur (int): La longeur de la liste

    Returns:
        list: Une liste de longeur longeur avec les deux premier éléments F et les autres T
    """ 
    res = []
    for indice in range(1,3):
        if indice <= longueur: 
            res.append(False)

    for indice in range(2,longueur):
        res.append(True)  
    return res 

def test_creer():
    assert creer(9) == [False, False, True, True, True, True, True, True, True]
    assert creer(0) == []
    assert creer(6) == [False, False, True, True, True, True]
    assert creer(1) == [False]

def trouver_multiple(liste, nb):
    """Cherche les indices en nombres premier dans un liste

    Args:
        liste (liste): Une liste de True et False
        nb (int): Multiple d'un nombre
    Returns:
        list: Une liste des multiples
    """
    for indice in range(2*nb, len(liste), nb):
        liste[indice] = False
    return liste

def test_trouver_multiple():
    assert trouver_multiple([],2) == []
    assert trouver_multiple([False,False,True,True,True],2) == [False,False,True,True,False]
    assert trouver_multiple([False,False,True,True,True],3) == [False,False,True,True,True]
    assert trouver_multiple([False,False,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True,True],3) == [False, False, True, True, True, True, False, True, True, False, True, True, False, True, True, False, True, True, False, True, True, False, True, True, False, True, True, False, True, True, False, True]

def eratosten(nb):
    
    res = []
    if nb >= 2:
        liste = creer(nb)
        for indice in range(2,len(liste)):
            liste = trouver_multiple(liste,indice)
        for indice in range(len(liste)):
            if liste[indice] == True:
                res.append(indice)
    return res

print(eratosten(10000))